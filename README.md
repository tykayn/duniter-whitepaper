# duniter-whitepaper

Rédaction d'un Whitepaper pour le projet Duniter.

Je ne suis pas compétent pour entrer dans les détails du projet. Cependant, je le suis assez pour rédiger la majeure partie du texte explicatif. J'invite qui le souhaite à m'accompagner dans cette$

### Références

D'autres whitepapers, pour observer le ton et les sujets à aborder.
* Bitcoin : https://bitcoin.org/bitcoin.pdf
* Ethereum : https://github.com/ethereum/wiki/wiki/White-Paper
* Nano : https://content.nano.org/whitepaper/Nano_Whitepaper_en.pdf

## Questions :

[la protection contre le spam](https://forum.duniter.org/t/sans-frais-de-transaction-comment-resister-aux-attaques/3846/25) a-t-elle été implémentée ? Sous quelle forme/paramètres ?



Plan
====

0. Abstract

1. Introduction

2. State of the art : Bitcoin case

2.1. Monetary creation of Bitcoin : a space-time asymmetry  
2.1.1. Spatial asymmetry  
2.1.2. Temporal asymmetry  
2.1.3. A solution  

2.2. Proof-of-Work mining : a power race (TODO)  

2.3. Other principles  
2.3.1. What about PoS ? (TODO)  
asymétrie spatio-temporelle  
2.3.2. What about DAG ? (TODO)  
projet Circles  


3. Duniter Blockchain (TODO)
Similar to Bitcoin.  

3.1. Duniters specificities  
3.1.1. WoT documents  
3.1.2. UD as monetary sources  

3.2. Spam protection  
3.2.1. Block dynamic size <!-- maybe in Scaling -->  
3.2.2. transactions maximal chainability  
3.3.3. amount tresholds  

3.3. Scaling  
* not an issue as many libre currencies might be created
* Lightning Network can be implemented
* automatic conversion between currencies thanks to RTM

4. Duniter Web of Trust  

4.1. Why do we need a Web of Trust ?  
4.1.1 Having our own certification system  

4.2. Fondamentals of graph theory  
4.2.1 A bit of vocabulary  
4.2.2 Definition of the Duniter Web of Trust  

4.3. Exploring the rules behind a Duniter Web of Trust  
4.3.1. Distance rule and referent members (`stepMax` and `xPercent`)  
4.3.2. Rule of the minimum number of certifications needed (`sigQty`)  
4.3.3. The membership renewal rule (`msValidity`, `msPeriod` and `msWindow`)  
4.3.4. Rule of certification lifespan   
4.3.5. Limitations of certifications (`sigValidity`, `sigStock` and `sigPeriod`)  
4.3.6. Pending certification and identity lifespan (`sigWindow` and `idtyWindow`)  

4.4. Details on some of the WoT's peculiarities at the genesis block.  

4.5.Why these rules and application cases in the Ğ1  
4.5.1. Distance and maximum size  
4.5.2. Time is our friend  
4.5.3. Trust me now, trust me forever? (`sigValidity`, `msValidity`)  
4.5.4. Keeping the pools free of information glut -(`idtyWindow`, `sigWindow`, `msWindow`)  
4.5.5. Avoiding single members from 'knowing too many people' (`sigStock`)  
4.5.6. Avoiding locking minorities (`xpercent`)  
4.5.7. Spam protection with (`msPeriod`)  


5. Proof of Work with individualized difficulty (TO BE CUT, TOO MUCH INFO ?)  

* use of PoW  
* Only members can mine

5.1. Principles of common difficulty (TO BE CUT)  
reminders of Bitcoin.  
5.1.1. Hash  
5.1.2. common difficulty  
5.1.3. How is difficulty applied  
5.1.4. The Nonce  

5.2. Personnalised difficulty  
5.2.1. Exclusion factor (exFact)  
5.2.2. The Handicap  

6. Conclusion
acknowledgements

7. Sources  
7.2. Duniter sources  
7.3. External sources  


