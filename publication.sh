#!/bin/bash

# Ce script sert à publier le WhitePaper.
# La première version crée uniquement un document whitepaper.html.
# Le doc Markdown complet est volontairement supprimé. Je préfère que les modifications soient faites sur les différentes parties.

# dépendances : pandoc

# ce script doit être lancé dans le dossier contenant les chapitres.

## Compilation des différents chapitres
rm whitepaper.md
touch whitepaper.md
for i in chapters/*.md.txt ; do
    cat $i >> whitepaper.md
done

## Création du whitepaper.html
rm whitepaper.html
pandoc -o whitepaper.html whitepaper.md

## suppression du whitepaper.md
#rm whitepaper.md

# git add files
git add whitepaper.md whitepaper.html
git add chapters/1_intro.md.txt chapters/2_looking_at_Bitcoin.md.txt chapters/3_blockchain.md.txt chapters/4_Web_Of_Trust.md.txt chapters/5_individualized_difficulty.md.txt chapters/6_conclusion.md.txt chapters/9_sources.md.txt 
